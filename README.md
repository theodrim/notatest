# not a test

0. Assumptions - deployed ubuntu 18.04 server with user `ubuntu` who have sudo permissions and interface named eth0 with network access.

1. Change `change_me` inside `hosts` to match your server ip. Beware that first task will change ip address, so you may need to restart play.
2. Decrypt vars with `ansible-vault decrypt vars/main.yml`.
3. Run as `ansible-playbook -i hosts main.yml` append `-K` if you need to provide password for elevated operations.
